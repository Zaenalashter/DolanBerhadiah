<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('dpn-1', function () {
    return view('dpn-smartphone');
});
Route::get('dpn-2', function () {
    return view('dpn-tvled');
});
Route::get('dpn-3', function () {
    return view('dpn-sepedamotor');
});
Route::get('dpn-4', function () {
    return view('dpn-mobil');
});
Route::get('1', function () {
    return view('smartphone');
});
Route::get('2', function () {
    return view('tvled');
});
Route::get('3', function () {
    return view('sepedamotor');
});
Route::get('4', function () {
    return view('mobil');
});
Route::get('win-1', function () {
    return view('win-smartphone');
});
Route::get('win-2', function () {
    return view('win-tvled');
});
Route::get('win-3', function () {
    return view('win-sepedamotor');
});
Route::get('win-4', function () {
    return view('win-mobil');
});
