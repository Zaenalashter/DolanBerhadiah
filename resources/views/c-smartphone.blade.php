<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('style/css/bootstrap.min.css') }}">

  <!-- My Font-->
  <link href="https://fonts.googleapis.com/css2?family=Sansita+Swashed:wght@800&display=swap" rel="stylesheet">

  <!-- My css-->
  <link rel="stylesheet" href="{{asset('../smartphone.css')}}">

  <title>Undian Saloka</title>
</head>

<body>
<div class="img">
  <a href="{{url('/')}}">
    <img src="{{asset('img/logo.png')}}" class="rounded float-left" alt="logo" >
  </a>
</div>

<!-- bg awal-->

      <div class="tab-tengah">
          <div class="container">
            <h1 class="display-4">SMARTPHONE</h1>
          
          <table class="table">
            <tbody>
              <tr>
                <th>1234567890</th>
              <tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-------------------------------------------------->
      

        
          <!--tabel bawah-->

              <div class="row">
                <div class="col tab-bawah">
                  <div class="tab1">
                    <table class="table">
                        <tbody>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                        </tbody>
                      </table>
                  </div>
                </div>
                <div class="col tab-bawah">
                <div class="tab2">
                    <table class="table">
                        <tbody>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                          <tr>
                            <td>1234567890</td>
                          <tr>
                        </tbody>
                      </table>
                    </div>
                </div>
                <div class="col tab-bawah">
                  <div class="tab3">
                      <table class="table">
                          <tbody>
                            <tr>
                              <td>1234567890</td>
                            <tr>
                            <tr>
                              <td>1234567890</td>
                            <tr>
                            <tr>
                              <td>1234567890</td>
                            <tr>
                            <tr>
                              <td>1234567890</td>
                            <tr>
                            <tr>
                              <td>1234567890</td>
                            <tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                  <div class="col tab-bawah">
                    <div class="tab4">
                        <table class="table">
                            <tbody>
                              <tr>
                                <td>1234567890</td>
                              <tr>
                              <tr>
                                <td>1234567890</td>
                              <tr>
                              <tr>
                                <td>1234567890</td>
                              <tr>
                              <tr>
                                <td>1234567890</td>
                              <tr>
                              <tr>
                                <td>1234567890</td>
                              <tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
              </div>
            </div>

          <div class="container">
            <div class="col tombol">
              <a href="" class="btn btn-danger">CARI PEMENANG</a>
            </div>
          </div>
               

          <!--akhir tabel bawah-->



<!-- bg akhir-->


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
    <script src="{{ asset('style/js/bootstrap.min.js') }}"></script>
</body>

</html>