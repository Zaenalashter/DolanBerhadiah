<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('style/css/bootstrap.min.css') }}">

  <!-- My Font-->
  <link href="https://fonts.googleapis.com/css2?family=Sansita+Swashed:wght@800&display=swap" rel="stylesheet">

  <!-- My css-->
  <link rel="stylesheet" href="{{asset('../home.css')}}">

  <title>Home Page</title>
</head>

<body>

<div class="pict-1">
<a href="">
<img src="{{asset('img/logo.png')}}" class="rounded float-left" alt="logo"  >
  </a>
</div>

<!-- bg awal-->
<div class="bgn-tengah">
        <div class="container">
            <h1 class="col display-4">SALOKA FEST 3.0</h1>
        </div>
        <div class="container">
            <h1 class="col display-5">CERIA</h1>
        </div>
        <div class="container">
          <h1 class="col display-6">DAPAT MOBIL</h1>
        </div>
        <div class="col display-7">
        <a href="{{url('/dpn-1')}}">
          <img src="{{asset('img/ikon-home.png')}}" class="rounded float-left" alt="logo"  >
        </a>
    </div>
</div>







<!-- bg akhir-->


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
    <script src="{{ asset('style/js/bootstrap.min.js') }}"></script>
</body>

</html>