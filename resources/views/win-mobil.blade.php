<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('style/css/bootstrap.min.css') }}">

  <!-- My Font-->
  <link href="https://fonts.googleapis.com/css2?family=Sansita+Swashed:wght@800&display=swap" rel="stylesheet">

  <!-- My css-->
  <link rel="stylesheet" href="{{asset('../win-mobil.css')}}">

  <title>Undian Saloka</title>
</head>

<body>
<div class="img">
<img src="{{asset('img/logo.png')}}" class="rounded float-left" alt="logo" >
</div>

<!-- bg awal-->

      <div class="tab-tengah">
          <div class="container">
            <h1 class="display-4">Selamat Kepada Pemenang</h1>
          
          <table class="table">
            <tbody>
              <tr>
                <th>123456789000000</th>
              <tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-------------------------------------------------->
      <div class="tab-bawah">
          <div class="container">
            <h1 class="display-4">Grand Prize</h1>
          </div>
          <div class="container">
            <h1 class="display-5">Mobil</h1>
          </div>
      </div>



<!-- bg akhir-->


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
    <script src="{{ asset('style/js/bootstrap.min.js') }}"></script>
</body>

</html>